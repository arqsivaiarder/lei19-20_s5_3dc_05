using System.Collections.Generic;
using mdf.Model.JoiningEntities;
using mdf.Model.Shared;
using mdf.Model.ValueObjects;

namespace mdf.Model
{
    public class Maquina : Entity, IAggregateRoot
    {
        public Maquina(string desc, string loc, TipoMaquina tipo) : base(desc)
        {
            Localizacao = new Localizacao
            {
                Value = loc
            };
            TipoMaquina = tipo;
            Ativada = true;
        }

        protected Maquina()
        {
        }

        public Localizacao Localizacao { get; set; }

        public TipoMaquina TipoMaquina { get; set; }

        public bool Ativada { get; set; }

        public IList<LinhaProducaoMaquina> LinhaProducaoMaquinas { get; set; } = new List<LinhaProducaoMaquina>();
    }
}